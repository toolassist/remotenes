const jsnes = require('jsnes')
const fs = require('fs')
const WebSocket = require('ws')
const {
  exit
} = require('process')

// Load ROM
const args = process.argv.slice(2)
if (args.length === 0) {
  console.error('Error reading rom: no filename provided')
  console.error('Usage: node index.js rompath.nes')
  exit()
}
const romData = fs.readFileSync(args[0], {
  encoding: 'binary'
})

const baseNES = new jsnes.NES()
// These calls come from outside the PPU
const ppuMethods = ['reset', 'startFrame', 'endFrame', 'startVBlank', 'setMirroring', 'updateControlReg1', 'updateControlReg2', 'writeSRAMAddress', 'sramWrite', 'scrollWrite', 'writeVRAMAddress', 'vramWrite', 'endScanline', 'spriteRamWriteUpdate']
// const apuMethods = ['writeReg']
// Enumerate method names for basic compression
var enumMap = new Map()
var i = 0
for (const property in baseNES.ppu) {
  if (baseNES.ppu[property] instanceof Function) {
    enumMap.set(property, i++)
  }
}
// enumMap.set('audioFrame', i++)
for (const property in baseNES.papu) {
  if (baseNES.papu[property] instanceof Function) {
    enumMap.set(property, i++)
  }
}

// Creates a mehtod call to be sent over the websocket
// Uses an ArrayBuffer to send it as binary
function createMessage(method, args, repeat) {
  const buffer = new ArrayBuffer(4 + args.length * 2) // 2 bytes for method, 2 for repeat, 2 per arg
  const methodView = new Uint16Array(buffer, 0, 1)
  const repeatView = new Uint16Array(buffer, 2, 1)
  const argsView = new Uint16Array(buffer, 4)
  methodView[0] = enumMap.get(method)
  repeatView[0] = repeat
  argsView.set(args)
  return buffer
}

// Start a NES emulator, and send the messages over the provided websocket
function startNES(ws) {
  var NES = new jsnes.NES()
  NES.papu.sampleRate = 48000
  NES.loadROM(romData)
  ws.send(JSON.stringify(NES.ppu.toJSON()))
  // Run-length encoding variables for (repeated) method calls
  var repeatCounter = 0
  var lastMethod = ''
  var lastArg = []

  // Proxy method calls of PPU
  for (const property in NES.ppu) {
    if (NES.ppu[property] instanceof Function) {
      if (ppuMethods.includes(property)) {
        // This is where the magic happens
        NES.ppu[property] = new Proxy(NES.ppu[property], {
          apply: function (target, thisArg, argArray) {
            // RLE
            if (property !== lastMethod || JSON.stringify(lastArg) !== JSON.stringify(argArray)) {
              if (repeatCounter > 0) {
                ws.send(createMessage(lastMethod, lastArg, repeatCounter))
              }
              lastMethod = property
              lastArg = argArray
              repeatCounter = 1
            } else {
              repeatCounter++
            }
            return Reflect.apply(target, thisArg, argArray) // Still actually apply the method
          }
        })
      }
    }
  }

  // Similar to PPU, without RLE
  NES.papu.writeAudio = false
  NES.papu.writeReg = new Proxy(NES.papu.writeReg, {
    apply: function (target, thisArg, argArray) {
      ws.send(createMessage('writeReg', argArray, 1))
      return Reflect.apply(target, thisArg, argArray)
    }
  })
  return NES
}

// Basic websocket server.
// Starts a NES emulator per session
const wss = new WebSocket.Server({
  port: 8989
})
const cont = jsnes.Controller
const controllerButtons = [cont.BUTTON_A, cont.BUTTON_B, cont.BUTTON_SELECT, cont.BUTTON_START, cont.BUTTON_UP, cont.BUTTON_DOWN, cont.BUTTON_LEFT, cont.BUTTON_RIGHT]
var controllerState = 0
var curActive = 0
wss.on('connection', ws => {
  curActive++
  if (curActive == 1) {
    var localNES = startNES(ws)
    var running = true
    const gameLoop = function () {
      const t1 = Date.now()
      localNES.frame()
      const t2 = Date.now()
      if (running) {
        const diff = t2 - t1
        if (diff > 0) {
          setTimeout(gameLoop, 1000 / 60 - diff)
        } else {
          setImmediate(gameLoop) // Need new frame now!
        }
      } else {
        localNES = null
      }
    }
    gameLoop()
  } else {
    ws.terminate()
  }
  ws.on('close', () => {
    clearInterval(pingInterval)
    running = false
    curActive--
  })

  // Handle controller
  ws.on('message', (message) => {
    const parsed = parseInt(message, 10)
    if (isNaN(parsed)) {
      return
    }
    if (parsed <= 65535) { // 2 bytes
      if (message !== controllerState) {
        for (var player = 0; player < 2; player++) {
          for (var i = 0; i < 8; i++) {
            const button = controllerButtons[i]
            const state = message >> (i + player * 8) & 1
            if (state === 0) {
              localNES.buttonUp(player + 1, button)
            } else {
              localNES.buttonDown(player + 1, button)
            }
          }
        }
        controllerState = message
      }
    }
  })

  var alive = true;
  var pingInterval = setInterval(() => {
    if (!alive) return ws.terminate()
    alive = false
    ws.ping()
  }, 30000)
  ws.on('pong', () => {
    alive = true
  })

})