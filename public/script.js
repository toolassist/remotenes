const canvas = document.querySelector('#player')
canvas.width = 256
canvas.height = 240
const ctx = canvas.getContext('2d')
const image = ctx.getImageData(0, 0, 256, 240)
ctx.fillStyle = 'black'
ctx.fillRect(0, 0, 256, 240)

var imgBuf = new ArrayBuffer(image.data.length)
var u8FrameBuf = new Uint8ClampedArray(imgBuf)
var u32FrameBuf = new Uint32Array(imgBuf)

var rebinding = false
var bindTarget = ''

// const audioBufSize = 8192
// let audioCurBuf = 0
// const audioL = new Float32Array(audioBufSize)
// const audioR = new Float32Array(audioBufSize)
// var audioRead = 0
// var audioWrite = 0
// const audioCtx = new window.AudioContext()
// const gainNode = audioCtx.createGain()
// gainNode.gain.value = 0.5
// const scriptNode = audioCtx.createScriptProcessor()
// scriptNode.connect(gainNode).connect(audioCtx.destination)
// scriptNode.onaudioprocess = event => {
//   const leftOut = event.outputBuffer.getChannelData(0)
//   const rightOut = event.outputBuffer.getChannelData(1)
//   const size = leftOut.length
//   if (!mute) {
//     while (audioCurBuf < size) { // Not enough samples in the buffer!
//       NES.papu.clockFrameCounter(2) // Generates samples until we have enough
//     }
//     for (i = 0; i < size; i++) {
//       leftOut[i] = audioL[audioRead]
//       rightOut[i] = audioR[audioRead]
//       audioRead = (audioRead + 1) % audioBufSize
//       audioCurBuf -= 1
//     }
//   } else {
//     for (i = 0; i < size; i++) {
//       leftOut[i] = 0
//       rightOut[i] = 0
//     }
//   }
// }

const NES = new jsnes.NES({
  // onAudioSample: (l, r) => {
  //   audioL[audioWrite] = l
  //   audioR[audioWrite] = r
  //   audioWrite = (audioWrite + 1) % audioBufSize
  //   audioCurBuf++
  // }
})
// Enumerate method names for basic compression
var enumMap = new Map()
var i = 0
for (const property in NES.ppu) {
  if (NES.ppu[property] instanceof Function) {
    enumMap.set(i++, property)
  }
}
// enumMap.set(i++, 'audioFrame')
for (const property in NES.papu) {
  if (NES.papu[property] instanceof Function) {
    enumMap.set(i++, property)
  }
}

function applyMessage(buffer) {
  const methodView = new Uint16Array(buffer, 0, 1)
  const repeatView = new Uint16Array(buffer, 2, 1)
  const argsView = new Uint16Array(buffer, 4)
  const method = enumMap.get(methodView[0])
  const nRepeat = repeatView[0]
  const args = argsView
  if (method !== undefined) {
    if (method === 'audioFrame') {
      const bufSize = (buffer.byteLength - 4) / (2 * 4)
      const leftView = new Float32Array(buffer, 4, bufSize)
      const rightView = new Float32Array(buffer, 4 + bufSize * 4, bufSize)
      for (var i = 0; i < bufSize; i++) {
        audioL[audioWrite] = leftView[i]
        audioR[audioWrite] = rightView[i]
        audioWrite = (audioWrite + 1) % audioBufSize
        audioCurBuf++
      }
    } else {
      for (i = 0; i < nRepeat; i++) {
        NES.ppu[method](...args)
      }
    }
  }
}

function setupPPU(state) {
  NES.ppu.fromJSON(JSON.parse(state))
  // Overwrite two exernal calls
  NES.ppu.nes.mmap = {
    clockIrqCounter: function () { },
    latchAccess: function (foo) { }
  }
  NES.ppu.nes.rom = {
    VERTICAL_MIRRORING: 0,
    HORIZONTAL_MIRRORING: 1,
    FOURSCREEN_MIRRORING: 2,
    SINGLESCREEN_MIRRORING: 3,
    SINGLESCREEN_MIRRORING2: 4,
    SINGLESCREEN_MIRRORING3: 5,
    SINGLESCREEN_MIRRORING4: 6,
    CHRROM_MIRRORING: 7
  }
  NES.ppu.writeReg = (addr, value) => {
    NES.papu.writeReg(addr, value)
  }
  // Drawing call!
  NES.ppu.nes.ui = {
    writeFrame: function (buffer) {
      for (var i = 0; i < 256 * 240 * 3; i++) {
        u32FrameBuf[i] = 0xFF000000 | buffer[i]
      }
      image.data.set(u8FrameBuf)
      ctx.putImageData(image, 0, 0)
    }
  }
}

const controllerButtons = ['A', 'B', 'Select', 'Start', 'Up', 'Down', 'Left', 'Right']
var p1Map = new Map([
  ['KeyJ', 'B'],
  ['KeyK', 'A'],
  ['Backspace', 'Select'],
  ['Enter', 'Start'],
  ['KeyW', 'Up'],
  ['KeyS', 'Down'],
  ['KeyA', 'Left'],
  ['KeyD', 'Right']
])
var p2Map = new Map([
  ['Numpad0', 'B'],
  ['NumpadDecimal', 'A'],
  ['NumpadAdd', 'Select'],
  ['NumpadEnter', 'Start'],
  ['ArrowUp', 'Up'],
  ['ArrowDown', 'Down'],
  ['ArrowLeft', 'Left'],
  ['ArrowRight', 'Right']
])
var buttonsChanged = false
var buttonStateP1 = 0
var buttonStateP2 = 0
const ws = new WebSocket('ws://remotenes.com/ws')
ws.binaryType = 'arraybuffer'
let firstMessage = true
ws.addEventListener('message', event => {
  if (firstMessage) {
    setupPPU(event.data)
    firstMessage = false
  } else {
    applyMessage(event.data)
  }
})
ws.addEventListener('close', () => {
  const err = document.createElement('h1');
  err.innerText = 'Server busy. Please try reloading to get allocated to a different server.'
  document.querySelector('body').prepend(err);
});
document.addEventListener('keydown', event => {
  if (p1Map.has(event.code) || p2Map.has(event.code || rebinding)) {
    event.preventDefault()
  }
  if (rebinding && event.code !== 'Tab') {
    bindTarget.value = event.code
    bindTarget.blur()
    rebinding = false
  }
  buttonHandler(event.code, true)
})
document.addEventListener('keyup', event => {
  if (p1Map.has(event.code) || p2Map.has(event.code)) {
    event.preventDefault()
  }
  buttonHandler(event.code, false)
})

function buttonHandler(key, direction) {
  const player = p1Map.has(key) ? 1 : p2Map.has(key) ? 2 : 0
  if (player === 0) return

  const keyMap = player === 1 ? p1Map : p2Map
  if (keyMap.has(key)) {
    const button = keyMap.get(key)
    changeState(player, button, direction)
  }
}

function changeState(player, button, pressed) {
  // Set buttonstate
  const bitIndex = Math.pow(2, controllerButtons.indexOf(button))
  const oldState = player === 1 ? buttonStateP1 : buttonStateP2
  var newState = oldState
  if (pressed) {
    newState |= bitIndex
  } else {
    newState &= 255 - bitIndex
  }

  // Update input viewer
  const svgDoc = document.querySelector(`#controller${player}`).getSVGDocument()
  if (svgDoc) {
    const svgButton = svgDoc.querySelector(`#${button.toLowerCase()}Button`)
    if (pressed) {
      svgButton.classList.add('active')
    } else {
      svgButton.classList.remove('active')
    }
  }

  // Apply changes
  if (newState !== oldState) {
    buttonsChanged = true
    if (player === 1) {
      buttonStateP1 = newState
    } else {
      buttonStateP2 = newState
    }
  }
}
// const audioStatus = document.querySelector('#volumeIndicator')
// let mute = true
// document.querySelector('#volume').addEventListener('input', function () {
//   gainNode.gain.value = this.value
//   if (this.value < 0.01) {
//     audioStatus.src = 'volume-mute.svg'
//     mute = true
//     audioCtx.suspend()
//   } else {
//     audioStatus.src = 'volume-up.svg'
//     if (mute) {
//       mute = false
//       audioCtx.resume()
//     }
//   }
// }, false)
// document.querySelector('#volumeIndicator').addEventListener('click', function () {
//   if (this.src.includes('mute')) {
//     if (gainNode.gain.value === 0) {
//       gainNode.gain.value = 0.05
//     }
//     document.querySelector('#volume').value = gainNode.gain.value
//     this.src = 'volume-up.svg'
//     mute = false
//     audioCtx.resume()
//   } else {
//     if (gainNode.gain.value === 0) {
//       gainNode.gain.value = 0.05
//     }
//     document.querySelector('#volume').value = 0
//     this.src = 'volume-mute.svg'
//     mute = true
//     audioCtx.suspend()
//   }
// }, false)
document.querySelector('#rebindP1').addEventListener('click', function () {
  createRebindMap(1)
})
document.querySelector('#rebindP2').addEventListener('click', function () {
  createRebindMap(2)
})

const controllerMap = new Map([
  [0, new Map([
    [0, [1, 'B']],
    [1, [1, 'A']],
    [8, [1, 'Select']],
    [9, [1, 'Start']],
    [12, [1, 'Up']],
    [13, [1, 'Down']],
    [14, [1, 'Left']],
    [15, [1, 'Right']]
  ])],
  [1, new Map([
    [0, [2, 'B']],
    [1, [2, 'A']],
    [8, [2, 'Select']],
    [9, [2, 'Start']],
    [12, [2, 'Up']],
    [13, [2, 'Down']],
    [14, [2, 'Left']],
    [15, [2, 'Right']]
  ])]
])

// Poll gamepads
setInterval(function () {
  const gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads : [])
  for (var i = 0; i < gamepads.length; i++) {
    if (gamepads[i]) {
      if (rebinding) {
        gamepads[i].buttons.forEach((value, idx) => {
          if (value.pressed) {
            bindTarget.value = `Controller ${i} button ${idx}`
            bindTarget.blur()
            rebinding = false
          }
        })
      }
      controllerMap.get(gamepads[i].index).forEach((value, buttonIdx) => {
        const direction = gamepads[i].buttons[buttonIdx].pressed
        changeState(value[0], value[1], direction)
      })
    }
  }
  if (buttonsChanged) {
    buttonsChanged = false
    ws.send(buttonStateP2 << 8 || buttonStateP1)
  }
}, 10)

function createKeyInput(key) {
  const input = document.createElement('input')
  input.setAttribute('type', 'text')
  input.setAttribute('value', key)
  return input
}

function createRebindMap(player) {
  checkForms()
  const keyboardButtons = player === 1 ? p1Map : p2Map
  const controllerBinds = []
  controllerMap.forEach((binds, idx) => {
    binds.forEach((bind, button) => {
      if (player === bind[0]) {
        controllerBinds.push([idx, button, bind[1]])
      }
    })
  })
  const form = document.querySelector(`#p${player}binds`)
  if (form.innerHTML === '') {
    form.innerHTML = '<small>To rebind, click and press a key or button on your controller. Don\'t forget to save!</small><br />'
    keyboardButtons.forEach((button, key) => {
      form.innerHTML += `<label>${button}</label>`
      form.appendChild(createKeyInput(key))
      form.innerHTML += '<br />'
    })
    controllerBinds.forEach(bind => {
      form.innerHTML += `<label>${bind[2]}</label>`
      form.innerHTML += `<input type="text" value="Controller ${bind[0]} button ${bind[1]}" /><br />`
    })
    const cancelButton = document.createElement('button')
    cancelButton.innerHTML = 'Cancel'
    cancelButton.addEventListener('click', (ev) => {
      ev.preventDefault()
      form.innerHTML = ''
    })
    const applyButton = document.createElement('button')
    applyButton.innerHTML = 'Save'
    applyButton.addEventListener('click', (ev) => {
      ev.preventDefault()
      if (applyRebindMap(player)) {
        ev.target.parentNode.innerHTML = ''
      }
    })
    form.appendChild(cancelButton)
    form.appendChild(applyButton)
  } else {
    form.innerHTML = ''
  }
}

function markDuplicate(player, key) {
  const form = document.querySelector(`#p${player}binds`)
  for (var i = 2; i < form.childNodes.length - 2; i += 3) {
    const curKey = form.childNodes[i + 1].value
    if (curKey === key) {
      form.childNodes[i + 1].classList.add('error')
    }
  }
}

function checkForms() {
  var valid = true
  var used = []
  for (var player = 1; player <= 2; player++) {
    const form = document.querySelector(`#p${player}binds`)
    // Check whether duplicate keybinds are set
    for (var i = 2; i < form.childNodes.length - 2; i += 3) {
      const key = form.childNodes[i + 1].value
      form.childNodes[i + 1].classList.remove('error')
      if (used.includes(key)) {
        markDuplicate(player, key)
        valid = false
      }
      used.push(key)
    }
  }
  return valid
}

function applyRebindMap(player) {
  const form = document.querySelector(`#p${player}binds`)
  // Check whether duplicate keybinds are set
  if (!checkForms()) {
    return false
  }

  // Remove all binds that exist for this player
  if (player === 1) {
    p1Map = new Map()
  } else {
    p2Map = new Map()
  }
  controllerMap.forEach(binds => {
    binds.forEach((button, bind) => {
      if (bind[0] === player) {
        binds.delete(button)
      }
    })
  })

  for (i = 2; i < form.childNodes.length - 2; i += 3) {
    const button = form.childNodes[i].innerText
    const key = form.childNodes[i + 1].value
    if (key.startsWith('Controller')) {
      const parts = key.split(' ')
      const controllerIdx = parseInt(parts[1])
      const buttonIdx = parseInt(parts[3])
      controllerMap.get(controllerIdx).set(buttonIdx, [player, button])
    } else {
      p1Map.set(key, button)
    }
  }
  return true
}
createRebindMap(1)
createRebindMap(2)
document.addEventListener('focusin', (ev) => {
  rebinding = true
  bindTarget = ev.target
  bindTarget.classList.add('focus')
})
document.addEventListener('focusout', (ev) => {
  ev.target.classList.remove('focus')
  checkForms()
})